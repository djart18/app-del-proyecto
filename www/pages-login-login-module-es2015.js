(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-login-login-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html":
/*!***********************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/login/login.page.html ***!
  \***********************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-header > \n  \n \n    <ion-row>\n       \n    <ion-col class=\"color2\">\n           <ion-title>    </ion-title> \n         \n    </ion-col>\n\n    <ion-col class=\"color\">\n\n       \n\n    </ion-col>\n    \n    </ion-row>\n\n\n</ion-header>\n\n<ion-content padding class=\"background\" >\n  <div >\n    \n  </div>\n\n<ion-row>\n  <div id=\"bloque\">\n  <ion-buttons  >\n    <div id=\"Df\">\n     \n        <ion-img  src=\"/assets/iteria.png\" ></ion-img>\n      \n    </div>  \n  </ion-buttons>\n</div>\n</ion-row>\n\n   <form #form=\"ngForm\">\n    <ion-grid>\n      <ion-row color=\"primary\" justify-content-center>\n        <ion-col align-self-center size-md=\"6\" size-lg=\"5\" size-xs=\"12\">\n          \n          <div padding   >\n            <ion-item >\n              <ion-input  type=\"email\"  name=\"email\" placeholder=\"your@email.com\"  id=\"email\" [(ngModel)]=\"email\" required minlength=5 ></ion-input>\n            </ion-item>\n            <ion-label></ion-label>\n            <ion-item  > \n              <ion-input  type=\"password\"  name=\"password\"  placeholder=\"Password\" id=\"password\"  [(ngModel)]=\"password\" required minlength=5  (mouseleave)=\"Llamada()\" ></ion-input>\n            </ion-item>    \n\n\n          </div>\n          <div padding >\n            <ion-button  size=\"large\" type=\"submit\" [disabled]=\"form.invalid\" (click)=\"Login()\"   expand=\"block\">Login</ion-button>\n          </div>\n\n          \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n      <div class=\"centrado\">\n      <ion-spinner  name=\"circles\" color=\"tertiary\"  *ngIf=\"show\"></ion-spinner>\n      </div>\n    </form>\n\n</ion-content>\n\n<ion-footer>\n \n    <ion-row>\n      <ion-col class=\"color2\">\n      </ion-col>\n      <ion-col class=\"color\">\n     </ion-col>\n     </ion-row>\n  \n</ion-footer>"

/***/ }),

/***/ "./src/app/pages/login/login.module.ts":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.module.ts ***!
  \*********************************************/
/*! exports provided: LoginPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPageModule", function() { return LoginPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _login_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./login.page */ "./src/app/pages/login/login.page.ts");







const routes = [
    {
        path: '',
        component: _login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]
    }
];
let LoginPageModule = class LoginPageModule {
};
LoginPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes)
        ],
        declarations: [_login_page__WEBPACK_IMPORTED_MODULE_6__["LoginPage"]]
    })
], LoginPageModule);



/***/ }),

/***/ "./src/app/pages/login/login.page.scss":
/*!*********************************************!*\
  !*** ./src/app/pages/login/login.page.scss ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "@charset \"UTF-8\";\n/* ion-item{\n    --background: #49ff38;\n    --color: #fff;\n} */\nion-button {\n  --background: #1158d1;\n}\n.centrado {\n  text-align: center;\n}\n.centrado {\n  text-align: center;\n}\n.color {\n  background-color: #ff6801;\n}\n.top-21 {\n  background-color: yellow;\n  position: relative;\n  left: 0;\n  right: 0;\n  margin: 0 auto;\n  top: 30%;\n  -webkit-transform: translateY(-50%);\n          transform: translateY(-50%);\n}\n.DF {\n  max-width: 125px;\n  height: 125px;\n  padding: 0.5em;\n  height: 12vh;\n  -webkit-filter: drop-shadow(3px 4px 4px #444);\n          filter: drop-shadow(3px 4px 4px #444);\n}\n.color2 {\n  background-color: #00297c;\n}\n.buttonn {\n  padding: 100px 20px;\n  width: 800px;\n}\n/* ion-content{\n    //background-color:color($colors,primary);\n    // background-image:url('…/assets/imagen.jpeg');\n    background-size:100% auto;\n    } */\n#bloque {\n  margin-left: auto;\n  margin-right: auto;\n  /* width: 100px; \n  height: 100px; */\n}\n.background {\n  --background: url('Imagen.png');\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9sb2dpbi9DOlxcVXNlcnNcXEFkbWluaXN0cmFkb3JcXERlc2t0b3BcXFRlbWFGaW5hbC9zcmNcXGFwcFxccGFnZXNcXGxvZ2luXFxsb2dpbi5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsZ0JBQWdCO0FDQWhCOzs7R0FBQTtBQUlBO0VBQ0kscUJBQUE7QURFSjtBQ0FBO0VBQ0ksa0JBQUE7QURHSjtBQ0ZDO0VBQ0csa0JBQUE7QURLSjtBQ0hBO0VBQ0UseUJBQUE7QURNRjtBQ0hBO0VBQ0ksd0JBQUE7RUFDQSxrQkFBQTtFQUNBLE9BQUE7RUFDQSxRQUFBO0VBQ0EsY0FBQTtFQUNBLFFBQUE7RUFDQSxtQ0FBQTtVQUFBLDJCQUFBO0FETUo7QUNIQTtFQUNJLGdCQUFBO0VBQ0EsYUFBQTtFQUNBLGNBQUE7RUFDQSxZQUFBO0VBQ0EsNkNBQUE7VUFBQSxxQ0FBQTtBRE1KO0FDSkE7RUFDSSx5QkFBQTtBRE9KO0FDTEE7RUFDSSxtQkFBQTtFQUNBLFlBQUE7QURRSjtBQ0xBOzs7O09BQUE7QUFRQTtFQUNJLGlCQUFBO0VBQ0Esa0JBQUE7RUFDQTtrQkFBQTtBRE1KO0FDRkE7RUFDSSwrQkFBQTtBREtKIiwiZmlsZSI6InNyYy9hcHAvcGFnZXMvbG9naW4vbG9naW4ucGFnZS5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGNoYXJzZXQgXCJVVEYtOFwiO1xuLyogaW9uLWl0ZW17XG4gICAgLS1iYWNrZ3JvdW5kOiAjNDlmZjM4O1xuICAgIC0tY29sb3I6ICNmZmY7XG59ICovXG5pb24tYnV0dG9uIHtcbiAgLS1iYWNrZ3JvdW5kOiAjMTE1OGQxO1xufVxuXG4uY2VudHJhZG8ge1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG59XG5cbi5jZW50cmFkbyB7XG4gIHRleHQtYWxpZ246IGNlbnRlcjtcbn1cblxuLmNvbG9yIHtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZmNjgwMTtcbn1cblxuLnRvcC0yMSB7XG4gIGJhY2tncm91bmQtY29sb3I6IHllbGxvdztcbiAgcG9zaXRpb246IHJlbGF0aXZlO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDtcbiAgbWFyZ2luOiAwIGF1dG87XG4gIHRvcDogMzAlO1xuICB0cmFuc2Zvcm06IHRyYW5zbGF0ZVkoLTUwJSk7XG59XG5cbi5ERiB7XG4gIG1heC13aWR0aDogMTI1cHg7XG4gIGhlaWdodDogMTI1cHg7XG4gIHBhZGRpbmc6IDAuNWVtO1xuICBoZWlnaHQ6IDEydmg7XG4gIGZpbHRlcjogZHJvcC1zaGFkb3coM3B4IDRweCA0cHggIzQ0NCk7XG59XG5cbi5jb2xvcjIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAyOTdjO1xufVxuXG4uYnV0dG9ubiB7XG4gIHBhZGRpbmc6IDEwMHB4IDIwcHg7XG4gIHdpZHRoOiA4MDBweDtcbn1cblxuLyogaW9uLWNvbnRlbnR7XG4gICAgLy9iYWNrZ3JvdW5kLWNvbG9yOmNvbG9yKCRjb2xvcnMscHJpbWFyeSk7XG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTp1cmwoJ+KApi9hc3NldHMvaW1hZ2VuLmpwZWcnKTtcbiAgICBiYWNrZ3JvdW5kLXNpemU6MTAwJSBhdXRvO1xuICAgIH0gKi9cbiNibG9xdWUge1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgbWFyZ2luLXJpZ2h0OiBhdXRvO1xuICAvKiB3aWR0aDogMTAwcHg7IFxuICBoZWlnaHQ6IDEwMHB4OyAqL1xufVxuXG4uYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKHNyY1xcYXNzZXRzXFxpbWdcXEltYWdlbi5wbmcpO1xufSIsIi8qIGlvbi1pdGVte1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjNDlmZjM4O1xyXG4gICAgLS1jb2xvcjogI2ZmZjtcclxufSAqL1xyXG5pb24tYnV0dG9ue1xyXG4gICAgLS1iYWNrZ3JvdW5kOiAjMTE1OGQxO1xyXG59XHJcbi5jZW50cmFkb3tcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufS5jZW50cmFkb3tcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG4uY29sb3J7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwxMDQsMSkgO1xyXG59XHJcblxyXG4udG9wLTIxe1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogeWVsbG93O1xyXG4gICAgcG9zaXRpb246cmVsYXRpdmU7XHJcbiAgICBsZWZ0OiAwO1xyXG4gICAgcmlnaHQ6IDA7XHJcbiAgICBtYXJnaW46MCBhdXRvO1xyXG4gICAgdG9wOjMwJTtcclxuICAgIHRyYW5zZm9ybTp0cmFuc2xhdGVZKC01MCUpO1xyXG5cclxufVxyXG4uREZ7XHJcbiAgICBtYXgtd2lkdGg6IDEyNXB4O1xyXG4gICAgaGVpZ2h0OiAxMjVweDtcclxuICAgIHBhZGRpbmc6IC41ZW07XHJcbiAgICBoZWlnaHQ6IDEydmg7XHJcbiAgICBmaWx0ZXI6IGRyb3Atc2hhZG93KDNweCA0cHggNHB4ICM0NDQpO1xyXG59XHJcbi5jb2xvcjJ7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiByZ2IoMCw0MSwxMjQpO1xyXG4gIH1cclxuLmJ1dHRvbm57XHJcbiAgICBwYWRkaW5nOiAxMDBweCAyMHB4O1xyXG4gICAgd2lkdGg6IDgwMHB4O1xyXG59XHJcblxyXG4vKiBpb24tY29udGVudHtcclxuICAgIC8vYmFja2dyb3VuZC1jb2xvcjpjb2xvcigkY29sb3JzLHByaW1hcnkpO1xyXG4gICAgLy8gYmFja2dyb3VuZC1pbWFnZTp1cmwoJ+KApi9hc3NldHMvaW1hZ2VuLmpwZWcnKTtcclxuICAgIGJhY2tncm91bmQtc2l6ZToxMDAlIGF1dG87XHJcbiAgICB9ICovXHJcblxyXG5cclxuXHJcbiNibG9xdWV7XHJcbiAgICBtYXJnaW4tbGVmdDogYXV0bztcclxuICAgIG1hcmdpbi1yaWdodDogYXV0bztcclxuICAgIC8qIHdpZHRoOiAxMDBweDsgXHJcbiAgICBoZWlnaHQ6IDEwMHB4OyAqL1xyXG59XHJcblxyXG4uYmFja2dyb3VuZHtcclxuICAgIC0tYmFja2dyb3VuZDogdXJsKHNyY1xcYXNzZXRzXFxpbWdcXEltYWdlbi5wbmcpO1xyXG59XHJcblxyXG5cclxuIl19 */"

/***/ }),

/***/ "./src/app/pages/login/login.page.ts":
/*!*******************************************!*\
  !*** ./src/app/pages/login/login.page.ts ***!
  \*******************************************/
/*! exports provided: LoginPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginPage", function() { return LoginPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var src_app_servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! src/app/servicios/beacons2.service */ "./src/app/servicios/beacons2.service.ts");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! src/app/services/authentication.service */ "./src/app/services/authentication.service.ts");
/* harmony import */ var _servicios_scanner_beacon_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../../servicios/scanner-beacon.service */ "./src/app/servicios/scanner-beacon.service.ts");







let LoginPage = class LoginPage {
    constructor(data, router, alertController, Service, Scanner) {
        this.data = data;
        this.router = router;
        this.alertController = alertController;
        this.Service = Service;
        this.Scanner = Scanner;
    }
    ngOnInit() {
    }
    Login() {
        this.show = true;
        this.data.newLogin(this.email, this.password).subscribe(data => {
            this.show = false;
        }, (error) => tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            this.show = false;
            const alert = yield this.alertController.create({
                subHeader: 'Error',
                message: 'Usuario o Contraseña incorrectos',
                buttons: ['OK']
            });
            yield alert.present();
        }));
    }
    Mostrar() {
        console.log('hsbhacbh');
        return true;
    }
    Llamada() {
        console.log('ah salido del input');
    }
};
LoginPage.ctorParameters = () => [
    { type: src_app_servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_2__["Beacons2Service"] },
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"] },
    { type: src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"] },
    { type: _servicios_scanner_beacon_service__WEBPACK_IMPORTED_MODULE_6__["ScannerBeaconService"] }
];
LoginPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-login',
        template: __webpack_require__(/*! raw-loader!./login.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/login/login.page.html"),
        styles: [__webpack_require__(/*! ./login.page.scss */ "./src/app/pages/login/login.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [src_app_servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_2__["Beacons2Service"],
        _angular_router__WEBPACK_IMPORTED_MODULE_3__["Router"],
        _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["AlertController"],
        src_app_services_authentication_service__WEBPACK_IMPORTED_MODULE_5__["AuthenticationService"],
        _servicios_scanner_beacon_service__WEBPACK_IMPORTED_MODULE_6__["ScannerBeaconService"]])
], LoginPage);



/***/ })

}]);
//# sourceMappingURL=pages-login-login-module-es2015.js.map