(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["pages-actividades-actividades-module"],{

/***/ "./node_modules/raw-loader/index.js!./src/app/pages/actividades/actividades.page.html":
/*!***********************************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/pages/actividades/actividades.page.html ***!
  \***********************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<ion-footer>\n  <!-- <ion-toolbar> -->\n  <ion-row>\n    <ion-col class=\"color2\">\n    </ion-col>\n    <ion-col class=\"color\">\n    </ion-col>\n  </ion-row>\n  <!-- </ion-toolbar>  -->\n</ion-footer>\n\n<ion-header>\n  <ion-toolbar color=\"medium\">\n    <ion-buttons slot=\"start\">\n      <ion-back-button defaultHref=\"/\"></ion-back-button>\n    </ion-buttons>\n\n    <ion-title>Detalle</ion-title>\n\n    <ion-buttons slot=\"end\">\n      <ion-buttons>\n        <ion-button (click)=\"mostrarPop( $event )\">\n          <ion-icon name=\"settings\" slot=\"start\"></ion-icon>\n          <ion-label> estado </ion-label>\n        </ion-button>\n      </ion-buttons>\n     \n    </ion-buttons>\n\n  </ion-toolbar>\n  <link href=\"actividades.page.scss\">\n</ion-header>\n\n<ion-content padding class=\"background\" >\n\n  <ion-icon   name=\"checkmark\"    *ngIf=\"sebas\" ></ion-icon>\n\n\n     <h3>{{created_at}}</h3>\n\n\n     <h1 align=\"center\" >{{proyecto}} </h1>\n      \n      <br>\n\n      <h2>{{name}}  -- {{ cliente }} </h2>\n\n\n      \n\n\n\n      <br>\n\n      <h3  >{{description}}</h3>\n\n\n      <br>\n\n      <h3  >{{ creacionUltima }}</h3>\n      \n      <ion-row>\n        <div id=\"bloque\">\n        <ion-buttons  >\n          <div class=\"Df\">\n            \n              <ion-img  src=\"/assets/iteria.png\" class=\"DF\" ></ion-img>\n            \n          </div>  \n        </ion-buttons>\n      </div>\n      </ion-row>\n\n\n      \n\n\n    <h1  class=\"Color-background\"  ></h1>\n\n  \n\n</ion-content>\n\n"

/***/ }),

/***/ "./src/app/pages/actividades/actividades.module.ts":
/*!*********************************************************!*\
  !*** ./src/app/pages/actividades/actividades.module.ts ***!
  \*********************************************************/
/*! exports provided: ActividadesPageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActividadesPageModule", function() { return ActividadesPageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "./node_modules/@angular/common/fesm2015/common.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _actividades_page__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./actividades.page */ "./src/app/pages/actividades/actividades.page.ts");







const routes = [
    {
        path: '',
        component: _actividades_page__WEBPACK_IMPORTED_MODULE_6__["ActividadesPage"]
    }
];
let ActividadesPageModule = class ActividadesPageModule {
};
ActividadesPageModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        entryComponents: [
        // PopinfoComponent
        ],
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_5__["IonicModule"],
            _angular_router__WEBPACK_IMPORTED_MODULE_4__["RouterModule"].forChild(routes),
        ],
        declarations: [_actividades_page__WEBPACK_IMPORTED_MODULE_6__["ActividadesPage"]]
    })
], ActividadesPageModule);



/***/ }),

/***/ "./src/app/pages/actividades/actividades.page.scss":
/*!*********************************************************!*\
  !*** ./src/app/pages/actividades/actividades.page.scss ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* .form-header{\n    width: 80px; height: 80px;\n} */\n.color {\n  background-color: #ff6801;\n}\n.color2 {\n  background-color: #00297c;\n}\n.background {\n  --background: url('imagen.png') center center fixed;\n  /*   background-size: cover;\n   -moz-background-size: cover;\n   -webkit-background-size: cover;\n   -o-background-size: cover;\n   position:absolute;\n  height: 10000%; */\n}\n#bloque {\n  margin-left: auto;\n  width: 60%;\n  height: 60%;\n  margin-top: 20px;\n}\n/* .DF{\n  width: 210px; height: 210px;\n} */\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvcGFnZXMvYWN0aXZpZGFkZXMvQzpcXFVzZXJzXFxBZG1pbmlzdHJhZG9yXFxEZXNrdG9wXFxUZW1hRmluYWwvc3JjXFxhcHBcXHBhZ2VzXFxhY3RpdmlkYWRlc1xcYWN0aXZpZGFkZXMucGFnZS5zY3NzIiwic3JjL2FwcC9wYWdlcy9hY3RpdmlkYWRlcy9hY3RpdmlkYWRlcy5wYWdlLnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7O0dBQUE7QUFlQTtFQUNFLHlCQUFBO0FDWEY7QURjQTtFQUNFLHlCQUFBO0FDWEY7QURjQTtFQUVFLG1EQUFBO0VBSUE7Ozs7O21CQUFBO0FDVkY7QURpQkE7RUFDRSxpQkFBQTtFQUVBLFVBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUNmRjtBRGlCQTs7R0FBQSIsImZpbGUiOiJzcmMvYXBwL3BhZ2VzL2FjdGl2aWRhZGVzL2FjdGl2aWRhZGVzLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIi8qIC5mb3JtLWhlYWRlcntcclxuICAgIHdpZHRoOiA4MHB4OyBoZWlnaHQ6IDgwcHg7XHJcbn0gKi9cclxuXHJcbiRjb2xvcnM6IChcclxuICBwcmltYXJ5OiAjNDg4YWZmLFxyXG4gIHNlY29uZGFyeTogIzMyZGI2NCxcclxuICBkYW5nZXI6ICNmNTNkM2QsXHJcbiAgbGlnaHQ6ICNmNGY0ZjQsXHJcbiAgZGFyazogIzIyMixcclxuICB0d2l0dGVyOiAoXHJcbiAgICBiYXNlOiAjNTVhY2VlLFxyXG4gICAgY29udHJhc3Q6ICNmZmZmZmZcclxuICApXHJcbik7XHJcbi5jb2xvciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDI1NSwgMTA0LCAxKTtcclxufVxyXG5cclxuLmNvbG9yMiB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogcmdiKDAsIDQxLCAxMjQpO1xyXG59XHJcblxyXG4uYmFja2dyb3VuZCB7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kOiB1cmwoc3JjXFxhc3NldHNcXGltZ1xcaXRlcmlhLWZvbmRvLmpwZWcpO1xyXG4gIC0tYmFja2dyb3VuZDogdXJsKHNyY1xcYXNzZXRzXFxpbWdcXGltYWdlbi5wbmcpIGNlbnRlciBjZW50ZXIgZml4ZWQ7XHJcbiAgLy8gLS1iYWNrZ3JvdW5kLXJlcGVhdDogcmVwZWF0LCBuby1yZXBlYXQ7XHJcbiAgLy8gYmFja2dyb3VuZC1zaXplOiAxMHBweCAxMHB4O1xyXG4gIFxyXG4gIC8qICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcclxuICAtbW96LWJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgLXdlYmtpdC1iYWNrZ3JvdW5kLXNpemU6IGNvdmVyO1xyXG4gIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XHJcbiAgcG9zaXRpb246YWJzb2x1dGU7XHJcbiBoZWlnaHQ6IDEwMDAwJTsgKi9cclxufVxyXG4jYmxvcXVle1xyXG4gIG1hcmdpbi1sZWZ0OiBhdXRvO1xyXG4gIC8vIG1hcmdpbi1yaWdodDogYXV0bztcclxuICB3aWR0aDogNjAlOyBcclxuICBoZWlnaHQ6IDYwJTtcclxuICBtYXJnaW4tdG9wOiAyMHB4O1xyXG59XHJcbi8qIC5ERntcclxuICB3aWR0aDogMjEwcHg7IGhlaWdodDogMjEwcHg7XHJcbn0gKi8iLCIvKiAuZm9ybS1oZWFkZXJ7XG4gICAgd2lkdGg6IDgwcHg7IGhlaWdodDogODBweDtcbn0gKi9cbi5jb2xvciB7XG4gIGJhY2tncm91bmQtY29sb3I6ICNmZjY4MDE7XG59XG5cbi5jb2xvcjIge1xuICBiYWNrZ3JvdW5kLWNvbG9yOiAjMDAyOTdjO1xufVxuXG4uYmFja2dyb3VuZCB7XG4gIC0tYmFja2dyb3VuZDogdXJsKHNyY1xcYXNzZXRzXFxpbWdcXGltYWdlbi5wbmcpIGNlbnRlciBjZW50ZXIgZml4ZWQ7XG4gIC8qICAgYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgIC1tb3otYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgIC13ZWJraXQtYmFja2dyb3VuZC1zaXplOiBjb3ZlcjtcbiAgIC1vLWJhY2tncm91bmQtc2l6ZTogY292ZXI7XG4gICBwb3NpdGlvbjphYnNvbHV0ZTtcbiAgaGVpZ2h0OiAxMDAwMCU7ICovXG59XG5cbiNibG9xdWUge1xuICBtYXJnaW4tbGVmdDogYXV0bztcbiAgd2lkdGg6IDYwJTtcbiAgaGVpZ2h0OiA2MCU7XG4gIG1hcmdpbi10b3A6IDIwcHg7XG59XG5cbi8qIC5ERntcbiAgd2lkdGg6IDIxMHB4OyBoZWlnaHQ6IDIxMHB4O1xufSAqLyJdfQ== */"

/***/ }),

/***/ "./src/app/pages/actividades/actividades.page.ts":
/*!*******************************************************!*\
  !*** ./src/app/pages/actividades/actividades.page.ts ***!
  \*******************************************************/
/*! exports provided: ActividadesPage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ActividadesPage", function() { return ActividadesPage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");
/* harmony import */ var _servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../../servicios/beacons2.service */ "./src/app/servicios/beacons2.service.ts");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "./node_modules/@ionic/angular/dist/fesm5.js");
/* harmony import */ var _components_popinfo_PopinfoComponent__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../../components/popinfo/PopinfoComponent */ "./src/app/components/popinfo/PopinfoComponent.ts");






// import { setTimeout } from 'timers';
let ActividadesPage = class ActividadesPage {
    constructor(route, popoverCtrl, PopinfoComponent, Beacons2Service) {
        this.route = route;
        this.popoverCtrl = popoverCtrl;
        this.PopinfoComponent = PopinfoComponent;
        this.Beacons2Service = Beacons2Service;
        this.tareas = [{
                name: '',
                description: '',
                cliente: '',
                proyecto: '',
                avance: '',
                creacionUltima: '',
                created_at: ''
            }];
        this.sebas = false;
        this.id = route.snapshot.params.id;
        this.name = route.snapshot.params.name;
        this.description = route.snapshot.params.description;
        this.estado = route.snapshot.params.estado;
        this.cliente = route.snapshot.params.cliente;
        this.proyecto = route.snapshot.params.proyecto;
        this.avance = route.snapshot.params.avance;
        this.creacionUltima = route.snapshot.params.creacionUltima;
        this.created_at = route.snapshot.params.created_at;
        console.log(this.created_at);
    }
    ngOnInit() {
    }
    igualar() {
        this.tareas;
    }
    Show_checkk(evento) {
        this.sebas = true;
    }
    mostrarPop(event) {
        return tslib__WEBPACK_IMPORTED_MODULE_0__["__awaiter"](this, void 0, void 0, function* () {
            const popover = yield this.popoverCtrl.create({
                component: _components_popinfo_PopinfoComponent__WEBPACK_IMPORTED_MODULE_5__["PopinfoComponent"],
                event: event,
                mode: 'ios',
                backdropDismiss: false
            });
            yield popover.present();
            const { data } = yield popover.onDidDismiss();
            console.log('Padre', data);
            if (data.item == 'Inicializado') {
                var actividad = 'Inicializado';
            }
            else if (data.item == 'Desarrollo') {
                var actividad = 'En desarrollo';
            }
            else {
                var actividad = 'Finalizado';
            }
            this.Beacons2Service.Update_tasks(this.id, this.name, this.description, this.cliente, this.avance, this.proyecto, actividad);
            //Desarrollo
            //finalizado
            //Inicializado
        });
    }
};
ActividadesPage.ctorParameters = () => [
    { type: _angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"] },
    { type: _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"] },
    { type: _components_popinfo_PopinfoComponent__WEBPACK_IMPORTED_MODULE_5__["PopinfoComponent"] },
    { type: _servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_3__["Beacons2Service"] }
];
ActividadesPage = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-actividades',
        template: __webpack_require__(/*! raw-loader!./actividades.page.html */ "./node_modules/raw-loader/index.js!./src/app/pages/actividades/actividades.page.html"),
        styles: [__webpack_require__(/*! ./actividades.page.scss */ "./src/app/pages/actividades/actividades.page.scss")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_2__["ActivatedRoute"], _ionic_angular__WEBPACK_IMPORTED_MODULE_4__["PopoverController"],
        _components_popinfo_PopinfoComponent__WEBPACK_IMPORTED_MODULE_5__["PopinfoComponent"],
        _servicios_beacons2_service__WEBPACK_IMPORTED_MODULE_3__["Beacons2Service"]])
], ActividadesPage);



/***/ })

}]);
//# sourceMappingURL=pages-actividades-actividades-module-es2015.js.map