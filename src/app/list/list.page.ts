import { Component, OnInit } from '@angular/core';
import { Beacons2Service } from '../servicios/beacons2.service';
import { Activity, Users } from '../models/Activity.1';
import { AlertController } from '@ionic/angular';
import { Router, ActivatedRoute, ActivatedRouteSnapshot } from '@angular/router';
import { AuthenticationService } from '../services/authentication.service';
import { Storage } from '@ionic/storage';

import * as moment from 'moment-timezone';
import { ScannerBeaconService } from '../servicios/scanner-beacon.service';
import { PushService } from '../servicios/push.service';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { PopinfoComponent } from '../components/popinfo/PopinfoComponent';


@Component({
  selector: 'app-list',
  templateUrl: 'list.page.html',
  styleUrls: ['list.page.scss']
})

export class ListPage implements OnInit {
  encapsular:any[]=[];
  refresh_activities:any[]=[];
  rotacion1 = 0;
  rotacion2=0;
  Completed_activities:any[]= [];
  marcus:boolean;
  sebas = true;
  permiso = 0;
  shaw = false ;
  show: boolean ;
  tamaño: any;
  bkUrl = {};
  fech: any;
  now: any;
  resto: any;
  jk: any;
  Mod: any;
  Mod1: number;
  id: number;
  background = "Imagen"
  Movement : number;
  presentaAlert: boolean = false;
  activities: Activity[];
  datos: Users[];
  todo: Users[];
  ordenar: any[];
  sebastian: Activity[];
  acoModar:number;
  lll:string;
  constructor(private data: Beacons2Service,
              private alertctrl: AlertController,
              private alertctrll: AlertController,
              private router: Router,
              private beacon2Service: Beacons2Service,
              private route: ActivatedRoute,
              private Service: AuthenticationService,
              private storage: Storage,
              private SStorage: Storage,
              private Scanner: ScannerBeaconService,
              private PushServicio: PushService,
              private popoverCtrl: PopinfoComponent
  ) {
    this.id = route.snapshot.params.id;
    this.PushServicio.Users();
    // this.Scanner.scan(this.id);
    this.shaw
  }

  onClick(){
    this.PushServicio.Push_notification();
  }
  // checked : boolean = true;

  onClickk( a ): void{
    console.log(a)
    console.log(a.selected)
  }
  
  

  Show_checkk(){
    console.log('Antes',this.permiso)
    this. permiso=1;
    console.log('Despues',this.permiso)
    this.shaw = true;
    
  }

  Actualizar(){
    this.shaw = false
    this.permiso = 0;
    this.sebas = false;
  }

  Search_for_tasks(){

  }


  ngOnInit() {
    
    this.beacon2Service.getActivities(this.id)  

      .subscribe(resp => {
        this.tamaño = resp.length;
        
        if (resp.length <= 0) {
          this.presentAlert()
        } else {
          this.activities = resp;
          var cantidad = resp.length
          var operacion = cantidad - 1;
          var FechaTarea = this.activities[operacion].created_at
          var Ffecha = FechaTarea.substring(0, 10)
          var f = new Date();
          var FechaActual = (f.getFullYear() + "-" + "0" + (f.getMonth() + 1) + "-" + f.getDate());

          if (FechaActual === Ffecha) {
            console.log('entro en la condicion')
            this.MensajeTarea();
            this.ordenarMethod();
          } else {
            this.ordenarMethod();
          }
        }
        this.Service.noReturn();
        this.refresh_page(this.tamaño);
      });
      this.shaw
  }

  borrar() {
    this.Service.logout();
  }

  getBkUrl() {
    const styles = {
      'background-image': "url(/assets/img/Imagen.png)"

    };
    console.log(styles);
    return styles;
  }

  async presentAlert() {
    const alert = await this.alertctrl.create({
      header: 'Alerta',
      message: 'No tienes actividades pendientes',
      buttons: ['OK']
    });

    await alert.present();
  }

  async MensajeTarea() {
    const alert = await this.alertctrll.create({
      header: 'Mensaje',
      message: 'Tienes actividades recientes',
      buttons: ['OK']
    });

    await alert.present();
  }


  refresh_page(Tamaño:number) {
    setTimeout(()=>{
      this.update_followed(this.id,Tamaño)
    },30000);
}

  update_followed(id:number,lengt:number){
    this.beacon2Service.getActivities(id)
    .subscribe(ressp => {
      var size = ressp.length
    this.load(lengt,size);
    });
  }

  load(current_size:number,size:number){
    setTimeout(() => {
      if(size>current_size){
        console.log('Tienes una nueva tarea')
        this.Movement = size;
        this.PushServicio.Push_notification();
      }else if (size == current_size){
        console.log('no tienes tareas por ahora')
        this.Movement = current_size;
      }
    }, 10000);
    this.refresh_page(this.Movement)  
  }

  doRefresh(event) {

    setTimeout(() => {
      // console.log('Actualizandoo');
      this.beacon2Service.getActivities(this.id)

      .subscribe(resp => {
        console.log('orden de las actividades:', resp)
        this.tamaño = resp.length;
        
        if (resp.length <= 0) {
          this.presentAlert()
        } else {
          this.activities = resp;
          var cantidad = resp.length
          var operacion = cantidad - 1;
          var FechaTarea = this.activities[operacion].created_at
          var Ffecha = FechaTarea.substring(0, 10)
          var f = new Date();
          var FechaActual = (f.getFullYear() + "-" + "0" + (f.getMonth() + 1) + "-" + f.getDate());

          if (FechaActual === Ffecha) {
            console.log('entro en la condicion')
            // this.MensajeTarea();
            this.ordenarMethod();
          } else {
            this.ordenarMethod();
          }
        }
      });
      event.target.complete();
    }, 1500);
  }

  acomodar(){
    var ggg = this.acoModar - 5;
    // console.log('Hora de la tarea: ',ggg );
  }
  ordenarMethod() {
    this.ordenar = this.activities.slice(0, this.activities.length);
    console.log('totas la actividades ',this.ordenar)
    var longitud = this.activities.length;
    var i = 0;
    var cantidad = 1;
    while (i < this.activities.length) {
      this.ordenar[i] = this.activities[longitud - cantidad]
      i++;
      cantidad++;
    }

    for (let i = 0; i < this.ordenar.length; i++) {
      this.now = this.ordenar[i].created_at;
      this.fech = this.now.substring(0, 11)
      console.log(this.fech)
      this.fech = moment(this.fech).format('DD MMM YYYY');
      var hora = this.now.substring(11, (this.now.length - 3))
      var Hora1 = hora.substring(0, 2)
      console.log(Hora1)
      this.acoModar = Hora1;
      this.resto = hora.substring(3, this.now.length)

      switch (Hora1) {
        case '13':
          this.Mod = '1:';
          this.Mod1 = 1;
          // this.Mod1 = this.Mod1-5;
          break;
        case '14':
          this.Mod = '2:';
          this.Mod1 = 2;
          // this.Mod1 = this.Mod1-5;
          break;
        case '15':
          this.Mod = '3:';
          this.Mod1 = 3;
          // this.Mod1 = this.Mod1-5;
          break;
        case '16':
          this.Mod = '4:';
          this.Mod1 = 4;
          // this.Mod1 = this.Mod1-5;
          break;
        case '17':
          this.Mod = '5:';
          this.Mod1 = 5;
          // this.Mod1 = this.Mod1-5;
          break;
        case '18':
          this.Mod = '6:';
          this.Mod1 = 6;
          // this.Mod1 = this.Mod1-5;
          break;
        case '19':
          this.Mod = '7:';
          this.Mod1 = 7;
          // this.Mod1 = this.Mod1-5;
          break;
        case '20':
          this.Mod = '8:';
          this.Mod1 = 8;
          // this.Mod1 = this.Mod1-5;
          break;
        case '21':
          this.Mod = '9:';
          this.Mod1 = 9;
          // this.Mod1 = this.Mod1-5;
          break;
        case '22':
          this.Mod = '10:';
          this.Mod1 = 10;
          // this.Mod1 = this.Mod1-5;
          break;
        case '23':
          this.Mod = '11:';
          this.Mod1 = 11;
          // this.Mod1 = this.Mod1-5;
          break;
        default:
          console.log('Lo lamentamos, por el momento no disponemos de ' + Hora1 + '.');
      }
      switch (this.Mod1) {
        case 1:
          this.lll = '1:';
          break;
        case 2:
          this.lll = '2:';
          break;
        case 3:
          this.lll = '3:';
          break;
        case 4:
            this.lll = '4:';
          break;
        case 5:
            this.lll = '5:';
          break;
        case 6:
            this.lll = '6:';
          break;
        case 7:
            this.lll = '7:';
          break;
        case 8:
            this.lll = '8:';
          break;
        case 9:
            this.lll = '9:';
          break;
        case 10:
            this.lll = '10:';
          break;
        case 11:
            this.lll = '11:';
          break;
        case 12:
            this.lll = '12:';
          break;
        default:
          // console.log('Lo lamentamos, por el momento no disponemos de ' + Hora1 + '.');
      }
      // this.Mod1 = this.Mod1-5;
      if (this.Mod1 == 7 || this.Mod1 == 8 || this.Mod1 == 9 || this.Mod1 == 10 || this.Mod1 == 11) {
        this.jk = 'am';
      } else {
        this.jk = 'pm';
      }
      var horaMod = this.lll + this.resto + " " + this.jk;
      var fin = this.fech + " / " + horaMod;
      this.ordenar[i].created_at = fin;
      // console.log(this.ordenar[i])
      this.acomodar();
    }

  }

  fecha_actual() {
    var f = new Date();
    var Fecha = (f.getFullYear() + "-" + "0" + (f.getMonth() + 1) + "-" + f.getDate());
    console.log(Fecha)
  }

  
Excuse_me(id:number,name: string, description: string,estado:string, proyecto: string, cliente: string, avance: number, created_at: any, a ){
  //  console.log( a ); 
  
  if(this.permiso == 0){
    this.abrirDetalles(id,name,description,estado,proyecto,cliente,avance,created_at);
    this.beacon2Service.capturarDatos(id, name , description, proyecto, cliente, avance)
    this.popoverCtrl.capturar(id,name,description,estado,proyecto,avance,cliente);
  }else {
    this.shaw = true;
  }
}


  abrirDetalles(id: number ,name: string, description: string,estado:string, proyecto: string, cliente: string, avance: number, created_at: any, ) {
    this.router.navigate(['/actividades', id, name, description,estado, proyecto, cliente, avance, created_at]);
    // this.Excuse_me();
  }

  DatosUserd(id: string, name: string) {
    this.router.navigate(['/actividades', id, name,])
  }

  logout() {
    this.storage.remove('USER_INFO').then(() => {
      this.Service.authState.next(false);
      this.router.navigate(['login']);
    });
  }


}
