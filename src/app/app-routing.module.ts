import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes, CanActivate } from '@angular/router';
import { Beacons2Service } from './servicios/beacons2.service';
import { AuthGuard } from './guards/auth.guard';
import { AuthGuard2Service } from './services/auth-guard2.service';
const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'list/:id', canActivate:[AuthGuard], 
    resolve: {
      pokemon:  Beacons2Service
    },
    loadChildren: () => import('./list/list.module').then(m => m.ListPageModule)
  },
  { path: 'actividades/:id/:name/:description/:estado/:proyecto/:cliente/:avance/:created_at', loadChildren: './pages/actividades/actividades.module#ActividadesPageModule' },
  { path: 'login', loadChildren: './pages/login/login.module#LoginPageModule',canActivate:[ AuthGuard2Service]}
  // canActivate:[AuthGuard]


];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
