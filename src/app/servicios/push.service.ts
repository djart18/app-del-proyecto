import { Injectable } from '@angular/core';
import { OneSignal } from '@ionic-native/onesignal/ngx';
import { HTTP } from '@ionic-native/http/ngx';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PushService {
  

  

  constructor(private oneSignal: OneSignal,
    
    private http: HttpClient) { }
    PlayerID:any;
  configuracioninicial(){
    this.oneSignal.startInit('2fb036bd-72a2-4315-93b2-7909efa35d66', '301101466517');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);
    
    this.oneSignal.handleNotificationReceived().subscribe(( noti ) => {
      console.log('Notificaion recibida', noti)
    });
    
    this.oneSignal.handleNotificationOpened().subscribe(( noti ) => {
      console.log('Notificacion abierta', noti )
    });
    this.oneSignal.endInit();

   
    

  }

  Users(){
    this.oneSignal.getIds().then(userid => {
      this.PlayerID = userid.userId;
      console.log(this.PlayerID)
    });
  }

  
Push_notification(){
   let Body = {
    
    "app_id": "2fb036bd-72a2-4315-93b2-7909efa35d66",
    "include_player_ids" : [this.PlayerID],
    "data": {"userid": "Postman-123" },
    "headings": { "en": "You have a new task pending", "es": "Tienes una nueva tarea pendiente" },
    "contents": { "en": "Open tso see more details", "es": "Abre para ver más detalles" }
  
  }
  let headers = {
    'Content-Type':  'application/json',
    'Authorization': "Basic MWY0M2ViNTgtYjA4ZC00MDQyLWEzNjQtNmY4NGI0OTJiOTMx",

  }
      this.http.post('https://onesignal.com/api/v1/notifications',Body, {headers: headers})
        .subscribe((resp) => console.log(resp));
  };

  Bienvenida(){
    let Body = {
     
     "app_id": "2fb036bd-72a2-4315-93b2-7909efa35d66",
     "include_player_ids" : [this.PlayerID],
     "data": {"userid": "Postman-123" },
     "headings": { "en": "Welcome to Iteria S.A.S", "es": "Bienvenido a Iteria S.A.S" },
     "contents": { "en": "Open to see details", "es": "Abre para ver más detalles" }
   
   }

   let headers = {
     'Content-Type':  'application/json',
     'Authorization': "Basic MWY0M2ViNTgtYjA4ZC00MDQyLWEzNjQtNmY4NGI0OTJiOTMx",
   }
       this.http.post('https://onesignal.com/api/v1/notifications',   Body , {headers: headers})
         .subscribe((resp) => console.log(resp));
   };

   Despedida(){
    let Body = {
     
     "app_id": "2fb036bd-72a2-4315-93b2-7909efa35d66",
     "include_player_ids" : [this.PlayerID],
     "data": {"userid": "Postman-123" },
     "headings": { "en": "Bye, thanks for your visit", "es": "Hasta pronto, gracias por su visita" },
     "contents": { "en": "Iteria S.A.S", "es": "Iteria S.A.S" }
   
   }

   let headers = {
     'Content-Type':  'application/json',
     'Authorization': "Basic MWY0M2ViNTgtYjA4ZC00MDQyLWEzNjQtNmY4NGI0OTJiOTMx",
   }
       this.http.post('https://onesignal.com/api/v1/notifications',   Body , {headers: headers})
         .subscribe((resp) => console.log(resp));
   };

  


  



}
