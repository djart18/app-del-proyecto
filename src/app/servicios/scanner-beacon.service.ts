import { Injectable, ViewChild } from '@angular/core';
import { IBeacon } from '@ionic-native/ibeacon/ngx';
import {HttpClient } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
import * as moment from 'moment';
import { LoginPage } from '../pages/login/login.page';
import { PushService } from './push.service';

@Injectable({
  providedIn: 'root'
})

export class ScannerBeaconService {
  
  
  delegate:any;
  
 
  constructor(private ibeacon: IBeacon,
              private http: HttpClient, 
              private HttpNative: HTTP,
              private pushService: PushService) {}
 
  scan(id: any){
 
    console.log("Inicializando Escaner de IBeacons!", 'ID DEL USUARIO:', id);
 
    
    this.ibeacon.requestAlwaysAuthorization()
    console.log(this.ibeacon)

    
    this.delegate = this.ibeacon.Delegate();
    console.log(this.delegate)

    
    this.ibeacon.requestWhenInUseAuthorization()
    
    this.delegate = this.ibeacon.Delegate();
 
    // Cambios en el estado de autorización
    this.delegate.didChangeAuthorizationStatus()
      .subscribe(data =>
        console.log('didChangeAuthorizationStatus has completed', JSON.stringify(data)))
        console.log()

    // Hizo un rango de los beacons que se encuentran en la región
    this.delegate.didRangeBeaconsInRegion()
      .subscribe(data => {
        console.log('didRangeBeaconsInRegion:', JSON.stringify(data,null,4))
          console.log(data.beacons[0].rssi);
    },
   
    // Manejo de errores
    error => console.log('didRangeBeaconsInRegion Error: ', error))

    // Comenzó a monitorear para la región
    this.delegate.didStartMonitoringForRegion()
      .subscribe(data =>
        console.log('didStartMonitoringForRegion:', JSON.stringify(data), 
          error => console.log(error)))
    
    // DEFINICION DE LA REGION DONDE SE UBICAN LOS BEACONS
    let Of203 = this.ibeacon.BeaconRegion('Oficina 203.beacon','f661bc05-1fd6-4115-9b8a-0f6ce5b7c475');
    let Of501 = this.ibeacon.BeaconRegion('Oficina 501.beacon','4c35b224-0858-4364-86b6-5b62c55f94f1');
    let Of505 = this.ibeacon.BeaconRegion('Oficina 505.beacon','3e74ebfa-5c4b-4a0a-9fc7-03e3e6975849');
    let OfSidney = this.ibeacon.BeaconRegion('Oficina Sidney.beacon','21f0d8c5-1506-4190-b2df-375af39c6d79');
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
    // EMPEZAR A MONITOREAR LAS DIFERENTES AREAS
    this.ibeacon.startMonitoringForRegion(Of203)
      .then(() => 
        console.log('Native layer received the request to monitoring:',JSON.stringify(Of203)),
          error => console.log('Native layer failed to begin monitoring: ', error));
 
    this.ibeacon.startMonitoringForRegion(Of501)
      .then(() => 
        console.log('Native layer received the request to monitoring:',JSON.stringify(Of501)),
          error => console.log('Native layer failed to begin monitoring: ', error));
 
    this.ibeacon.startMonitoringForRegion(Of505)
      .then(() => 
        console.log('Native layer received the request to monitoring:',JSON.stringify(Of505)),
          error => console.log('Native layer failed to begin monitoring: ', error));
 
    this.ibeacon.startMonitoringForRegion(OfSidney)
      .then(() => 
        console.log('Native layer received the request to monitoring:',JSON.stringify(Of505)),
          error => console.log('Native layer failed to begin monitoring: ', error));
  ////////////////////////////////////////////////////////////////////////////////////////////////////
 
  // SI ENTRÓ A UNA REGIÓN
 
  this.delegate.didEnterRegion()
    .subscribe(data => {
      let  headers = {
        'Content-Type': 'application/json',
        'responseType': 'json',
      };
    console.log('Entró en la Region: ', data);
    
    // MANEJO DE FECHAS PARA LOS CHECKS

    let now = moment(new Date());
    let date = now.format("YYYY-MM-DD HH:mm:ss").toLocaleString();
    let uuid = data.region.uuid;
    
    console.log('Fecha y hora de entrada:', date);
    console.log('Beacon ID:', uuid);

    // ENVIAR DATA PARA LA CREACIÓN DE LOS CHECKS
 
    this.http.post('http://127.0.0.1:8000/api/checktest', {'check_in': date, 'beacon_uuid': uuid, 'uid': id}, {headers: headers})
      .subscribe(resp => {
      console.log(resp)
      });
      this.pushService.Bienvenida();
    });
 
    // SI SALIÓ DE UNA REGIÓN

    this.delegate.didExitRegion()
      .subscribe(data => {
        let  headers = {
          'Content-Type':  'application/json',
          'responseType': "json",
        };
      console.log('Salió de laRegion: ', data);
      
      // MANEJO DE FECHAS PARA LOS CHECKS

      let now = moment(new Date());
      let date = now.format("YYYY-MM-DD HH:mm:ss").toLocaleString();
      let uuid = data.region.uuid;
 
      console.log('Fecha y hora de salida:', date);
      console.log('Beacon ID:', uuid);
      console.log( 'Data Region UUID:', data.region.uuid);
 
      // ENVIAR DATA PARA LA CREACIÓN DE LOS CHECKS

      this.http.post('http://127.0.0.1:8000/api/checktest', {'check_out': date, 'beacon_uuid': uuid, 'uid': id}, {headers: headers})
        .subscribe((resp) => 
         console.log(resp));
         this.pushService.Despedida();
      });
    }

  
}
