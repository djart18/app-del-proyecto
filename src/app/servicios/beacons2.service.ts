import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Activity, Users } from '../models/Activity.1';
import { Resolve, Router, ActivatedRouteSnapshot } from '@angular/router';
import { throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthenticationService } from '../services/authentication.service';
import { ScannerBeaconService } from './scanner-beacon.service';

@Injectable({
  providedIn: 'root'
})
export class Beacons2Service  implements Resolve<any>{

  headers = new HttpHeaders({
    'Content-Type': 'application/json',
    'responseType': 'json',
  });

  uid: any;
  errorData: {};

  constructor(private http: HttpClient,
              private data: Beacons2Service,
              private Service: AuthenticationService,
              private Scanner: ScannerBeaconService) { }

  private api='http://127.0.0.1:8000/api/Activities';

  // private api1='http://laravel.test/login?email=';
  // private api2='http://laravel.test/login?email=psenger@example.com';


  capturarDatos(id: number, name: string, description: string, proyecto: string, cliente: string, avance: number)  {
    console.log(id, name, description, proyecto, cliente, avance);
  }

  resolve( route: ActivatedRouteSnapshot){
    let id = route.paramMap.get('id');    
  }

  actividad(){
    return this.http.get<Activity[]>('http://127.0.0.1:8000/api/Activities');
  }

  getTask(id:string){
    const path = `${this.api}/${id}`;
    return this.http.get<Activity>(path);
  }

  newLogin(email: string, password: string) {
    return this.http.post('http://127.0.0.1:8000/api/login', {email: email, password: password}).pipe(map(resp => {
        console.log('resp', resp);
 
        if(resp){
    
          this.uid = resp[0].id;
          // this.Scanner.scan(this.uid);
          console.log('resp_exist', resp);
    
        this.CArgar(this.uid, email, password)
        
        }
      }),
      catchError(this.handleError)
    );
  }

  
  
  Update_tasks(id:any, name: string, description:string, cliente: string, avance:any, proyecto:any,actividad:string){
      return this.http.put('http://127.0.0.1:8000/api/Activities/${id}?',{ name:name, description: description, cliente: cliente, estado: actividad, avance:avance, proyecto:proyecto}).subscribe(resp =>{
        console.log(resp)
      });
  }

  CArgar(uid: any, email: string, password: string) {
      this.Service.login(uid, email, password)
  }
  
  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
 
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
 
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong.
      console.error(`Backend returned code ${error}, ` + `body was: ${error}`);
    }
 
    // return an observable with a user-facing error message
    this.errorData = {
      errorTitle: 'Oops! Request for document failed',
      errorDesc: 'Something bad happened. Please try again later.'
    };
    return throwError(this.errorData);
  }

  getActivities(id: number) {
    return this.http.get<Activity[]>(`http://127.0.0.1:8000/${id}`);
  }

}
