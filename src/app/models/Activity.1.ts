export interface Activity {
    name?: string;
    estado?: string;
    description?: string;
    avance?: number;
    cliente?: string;
    proyecto?: string;
    d_name?: string;
    created_at?:string;
    Hora?:any;
    selected?:boolean;
}

export interface Users{
    id: number;
    name: string;
    email: string;
    password:string
}

