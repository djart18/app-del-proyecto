import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { elementEventFullName } from '@angular/compiler/src/view_compiler/view_compiler';
import { Location } from '@angular/common';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { HTTP } from '@ionic-native/http/ngx';
// import {HttpClient,HttpHeaders } from '@angular/common/http';
import { ScannerBeaconService } from '../servicios/scanner-beacon.service';

@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  authState = new BehaviorSubject(false);
  dummy_reponse: any;
  protected = new BehaviorSubject(true);
  Respuesta: any;
  hola2 = false;

  Activate_Route(){
     return this.protected.value;
  }

  constructor( private router: Router, 
               private storage: Storage, 
               private platform: Platform, 
               private toastController: ToastController, 
               private location: Location, 
               private http: HttpClient, 
               private alertController: AlertController, 
               private httpp: HTTP, 
               private beacons:  ScannerBeaconService
  ) {
    this.platform.ready().then(() => {
      this.ifLoggedIn();
    });
  }

  ifLoggedIn() {
    this.storage.get('USER_INFO').then((response) => {
      if (response) {
        this.authState.next(true);

        return response;
      }
    });
  }


  async presentAlert() {
    const alert = await this.alertController.create({
      // header: 'Alert',
      subHeader: 'Error',
      message: 'Contraseña o coreo incorrecto',
      buttons: ['OK']
    });

    await alert.present();
  }

  isAuthenticated() {
    return this.authState.value;
  }

  login(uid: string, email: string, password: string) {

    this.dummy_reponse = {
      user_email: email,
      user_password: password,
      user_id: uid
    };

    this.http.post<Response>('http://127.0.0.1:8000/api/user', { email: email, password: password }, {})
      .subscribe((resp) => {
        
        if (resp) {
          console.log(resp);
          this.authState.next(true);
          console.log('Autorizado');
          this.router.navigate(['/list', uid,]);
          this.storage.set('USER_INFO', this.dummy_reponse).then((resp) => {
          console.log(resp);
          });
        } 
      });
    }

  logout() {
    this.storage.remove('USER_INFO').then(() => {
      this.router.navigate(['/login']);
      this.authState.next(false);
    });
  }

  noReturn(): boolean {
     this.storage.get('USER_INFO').then((response)=>{
      this.Respuesta = response;
      });

       if (this.Respuesta) {
       this.protected.next(false);
      
      } else {
       this.protected.next(true);
      }
      console.log(this.protected.value);
      return this.protected.value;
  }

 

}
