import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { Observable, BehaviorSubject } from 'rxjs';
import { Storage } from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard2Service implements CanActivate {
  protected = new BehaviorSubject(false);

  constructor(
    public authenticationService: AuthenticationService,
    private storage: Storage,
    // private Router : Router
  ) { }

  canActivate(): boolean {

     return this.authenticationService.noReturn();


  }

}

