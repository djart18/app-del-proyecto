import { Component, OnInit } from '@angular/core';
import { Beacons2Service } from 'src/app/servicios/beacons2.service';
import { Users, Activity } from '../../models/Activity.1';
import { Router, ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { AuthenticationService } from 'src/app/services/authentication.service';
import { ScannerBeaconService } from '../../servicios/scanner-beacon.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})

export class LoginPage implements OnInit {

  sebastain : Activity;
  advertencia: any;
  show: boolean;
  email: string;
  password: string;
  todo: any;
  nombre: any;
  uid: any;

  constructor( private data: Beacons2Service,
               private router: Router,
               private alertController: AlertController,
               private Service: AuthenticationService,
               private Scanner: ScannerBeaconService) {
  }

  ngOnInit() {

  }

  Login() {
    this.show = true;
    this.data.newLogin(this.email, this.password).subscribe(data => {
      this.show = false;
    },
    async error => {
      this.show = false;
      const alert = await this.alertController.create({
        subHeader: 'Error',
        message: 'Usuario o Contraseña incorrectos',
        buttons: ['OK']
      });
  
      await alert.present();
 
    });
    }

  Mostrar() {
    console.log('hsbhacbh')
    return true
  }

  Llamada(){
    console.log('ah salido del input')
  }


}
