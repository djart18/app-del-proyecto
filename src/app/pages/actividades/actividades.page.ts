import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Beacons2Service } from '../../servicios/beacons2.service';
import { Activity } from "src/app/models/Activity.1";
import { PopoverController, ToastController } from '@ionic/angular';
import { PopinfoComponent } from "../../components/popinfo/PopinfoComponent";
import { LoginPage } from '../login/login.page';
// import { setTimeout } from 'timers';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.page.html',
  styleUrls: ['./actividades.page.scss'],
})
export class ActividadesPage implements OnInit {
  tareas:any[]=[{
    name:'',
    description:'',
    cliente:'',
    proyecto:'',
    avance:'',
    creacionUltima:'',
    created_at:''
  }]

  sebas = false;
  id: string;
  name: string;
  description: string;
  estado:string;
  cliente: string;
  proyecto: string;
  avance: string;
  creacionUltima: Date;
  created_at: any;
  
  constructor(private route: ActivatedRoute,private popoverCtrl: PopoverController,
              private PopinfoComponent: PopinfoComponent,
              private Beacons2Service:Beacons2Service) 
    { 
    this.id = route.snapshot.params.id;
    this.name = route.snapshot.params.name;
    this.description = route.snapshot.params.description;
    this.estado = route.snapshot.params.estado;
    this.cliente = route.snapshot.params.cliente;
    this.proyecto = route.snapshot.params.proyecto;
    this.avance = route.snapshot.params.avance;
    this.creacionUltima = route.snapshot.params.creacionUltima;
    this.created_at = route.snapshot.params.created_at;
    console.log(this.created_at)
    
  }

  ngOnInit() {
  }

  igualar(){
    this.tareas
  }

  Show_checkk( evento ){
  this.sebas = true;
  } 

   async mostrarPop(event){
    const popover = await this.popoverCtrl.create({
      component: PopinfoComponent,
      event: event,
      mode: 'ios',
      backdropDismiss: false
     
    });
    await popover.present();
    
    const {data } = await popover.onDidDismiss();

    console.log('Padre', data);
     if(data.item == 'Inicializado'){
      var actividad = 'Inicializado'
      }else if(data.item == 'Desarrollo'){
      var actividad = 'En desarrollo'
      }else{
        var actividad = 'Finalizado'
      }
      this.Beacons2Service.Update_tasks(this.id,this.name,this.description,this.cliente,this.avance,this.proyecto,actividad);
    //Desarrollo
    //finalizado
    //Inicializado
  }



  
  

  
}
