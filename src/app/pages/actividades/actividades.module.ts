import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule, PopoverController } from '@ionic/angular';

import { ActividadesPage } from './actividades.page';
import { PopinfoComponent } from "../../components/popinfo/PopinfoComponent";
import { popoverController } from '@ionic/core';

const routes: Routes = [
  {
    path: '',
    component: ActividadesPage
  }
];

@NgModule({
  entryComponents:[
    // PopinfoComponent
    ],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ],
  declarations: [ActividadesPage]
})
export class ActividadesPageModule {}
