import { Component, OnInit, Injectable } from '@angular/core';
import { PopoverController, ToastController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Beacons2Service } from '../../servicios/beacons2.service';


@Component({
  selector: 'app-popinfo',
  templateUrl: './popinfo.component.html',
  styleUrls: ['./popinfo.component.scss'],
})
@Injectable({
  providedIn: 'root',
})
export class PopinfoComponent implements OnInit {
  objetivo = false;
  arreglo2: any[];
  arreglo: any[] = [{
    id: 'ffff',
    name: 'ffff',
    descrption: 'ffff',
    estado: 'fff',
    proyecto: 'fff',
    avance: 'fff',
    cliente: 'fff'
  }]

  paso = false;
  public id: any;
  public name: any;
  public description: any;
  public estado: any;
  public proyecto: any;
  public avance: any;
  public cliente: any;

  capturar(id: number, name: string, description: string, estado: string, proyecto: string, avance: any, cliente: string): any {
    this.id = id;
    this.name = name;
    this.description = description;
    this.estado = estado;
    this.proyecto = proyecto;
    this.avance = avance;
    this.cliente = cliente;
    
  }



  item = [];

  Opcionnes: any[] = [

    {
      name: 'Inicializado',
      icon: 'clipboard'
    },
    {
      name: 'Desarrollo',
      icon: 'build'
    },
    {
      name: 'Finalizado',
      icon: 'checkmark'
    }
  ];

  constructor(private popoverCtrl: PopoverController,
    private toastController: ToastController,
    private http: HttpClient,
    private Beacons2Service: Beacons2Service
  ) { }

  ngOnInit() { }
  onClick(item:string) {
    this.objetivo = true

    console.log('item:',item)
    this.popoverCtrl.dismiss({
      item: item
    }); 
  /* this.objetivo = true;
  console.log(this.objetivo)  */ 
    /* console.log('lo que guardo en el arreglo 2', this.arreglo2)
    this.popoverCtrl.dismiss(); */

    if (item == 'Inicializado') {
      var actividad = 'La tarea ha sido inicializada'
      var mosca = 'Inicializado'

    } else if (item == 'Desarrollo') {
      var actividad = 'La tarea se encuentra en desarrollo'
      var mosca = 'En desarrollo'
    } else if (item) {
      var actividad = 'La tarea ha sido finalizada'
      var mosca = 'Finalizado';
    }
    this.presentToast(actividad);
  }

  async presentToast(Actividad: string) {
    const toast = await this.toastController.create({
      message: Actividad,
      duration: 2000
    });
    toast.present();
  }


}
