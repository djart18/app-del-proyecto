import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthenticationService } from './services/authentication.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { ScannerBeaconService } from './servicios/scanner-beacon.service';
import { PushService } from './servicios/push.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers : [ ScannerBeaconService ]  //Impotando providers
})
export class AppComponent {

  id: any;
  reply:any;
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private authenticationService: AuthenticationService,
    private router: Router,
    private storage: Storage,
    private PushServicio: PushService

  ) {
    this.initializeApp();
  }

  initializeApp() {
      this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      this.PushServicio.configuracioninicial();
      
      this.authenticationService.authState.subscribe(state =>{
      if(state){

        this.storage.get('USER_INFO').then(resp => {
        this.router.navigate([`/list/${resp.user_id}`])
        });
        

      }else{
        this.router.navigate(['/login'])
      }
      
      })
    });
  }
}
