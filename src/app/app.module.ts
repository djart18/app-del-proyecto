import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { HTTP } from '@ionic-native/http/ngx';
import { IonicModule, IonicRouteStrategy} from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { IonicStorageModule } from '@ionic/storage';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { AuthGuard } from './guards/auth.guard';
import { AuthenticationService } from './services/authentication.service';
import { ScannerBeaconService } from './servicios/scanner-beacon.service';
import { OneSignal } from '@ionic-native/onesignal/ngx'; 
import { IBeacon } from '@ionic-native/ibeacon/ngx';
import { PopinfoComponent } from './components/popinfo/PopinfoComponent';

@NgModule({
  declarations: [AppComponent,PopinfoComponent],
  entryComponents: [PopinfoComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    IonicStorageModule.forRoot(),
    HttpClientModule
  ],
  providers: [ 
    PopinfoComponent, 
    StatusBar,
    SplashScreen,
    AuthGuard,
    AuthenticationService,
    HTTP,
    OneSignal,
    ScannerBeaconService,
    IBeacon,
  
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  onClick(){
    console.log('chsbadchabs');
  }
}

